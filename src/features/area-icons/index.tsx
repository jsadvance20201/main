import * as React from 'react';

import { Wrapper, IconsBlock } from './styled';

export const AreaIcons = (props: any) => {
    const imgUrl = `static/region/${'CAD_1'}.png`;
    return (
        <Wrapper>
            <div>
                <IconsBlock>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/01-akademichecky.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/02-gagarinsky.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/03-zuzino.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/04-konkovo.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/05-kotlovka.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/06-lomonosovsk.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                </IconsBlock>
                <IconsBlock>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/07-obruchevsky.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/08-servnoe-butovo.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/09-tyoply-stan.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/10-cheryomushki.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/11-uzhnoe-butovo.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                    <div>
                        <img
                            src={`${__webpack_public_path__}static/region/12-yasenevo.png`}
                            alt="Арбат"
                            onError={(err) => console.info(err)}
                        />
                    </div>
                </IconsBlock>
            </div>
        </Wrapper>
    );
};
