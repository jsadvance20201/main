import styled from 'styled-components';
import { device } from '@main/__data__/constants';

export const SocialTitle = styled.div`
    font-size: 22px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    text-align: center;
    color: #1e1d20;

    span:first-child {
        display: none;
    }

    @media ${device.tablet} {
        span:first-child {
            font-size: 38px;
            display: inline;
        }

        span:nth-child(2) {
            display: none;
        }
    }
`;

export const IconstBlock = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: baseline;
    height: 100px;
    padding-top: 12px;

    img {
        width: 32px;
    }

    a:nth-child(n+2) {
        margin-left: 24px;
    }

    @media ${device.tablet} {
        height: 192px;
        padding-top: 0;
        align-items: center;

        img {
            width: 64px
        }

        a:nth-child(n+2) {
            margin-left: 48px;
        }
    }
`;
