import React from 'react';

export const AreaIcons = React.lazy(() =>
  import(/* webpackChunkName: "area-icons" */ './area-icons')
    .then(({ AreaIcons: Component }) => ({ default: Component })),
);

export const Competitions = React.lazy(() =>
  import(/* webpackChunkName: "competitions" */ './competitions')
    .then(({ Competitions: Component }) => ({ default: Component })),
);

export const GallarySlider = React.lazy(() =>
  import(/* webpackChunkName: "gallary-slider" */ './gallary-slider')
    .then(({ GallarySlider: Component }) => ({ default: Component })),
);

// export const MapWithForm = React.lazy(() =>
//   import(/* webpackChunkName: "map-with-form" */ './map-with-form')
//     .then(({ MapWithForm: Component }) => ({ default: Component })),
// );

export const SocialSection = React.lazy(() =>
  import(/* webpackChunkName: "social" */ './social')
    .then(({ SocialSection: Component }) => ({ default: Component })),
);

export const TopSlider = React.lazy(() =>
  import(/* webpackChunkName: "top-slider" */ './top-slider')
    .then(({ TopSlider: Component }) => ({ default: Component })),
);

export const VideoSection = React.lazy(() =>
  import(/* webpackChunkName: "video-section" */ './video-section')
    .then(({ VideoSection: Component }) => ({ default: Component })),
);