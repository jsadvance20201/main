import * as React from 'react';

import {
    Wrapper, CompetitionsTitle, CompetiotionDate, CompetiotionMonth, CompetiotionName, Circle, CompetiotionBlock
} from './styled';

export const Competitions = (props: any) => (
    <Wrapper>
        <CompetitionsTitle>Соревнования</CompetitionsTitle>
        <CompetiotionBlock>
            <div>
                <CompetiotionDate>12</CompetiotionDate>
                <CompetiotionMonth>декабря</CompetiotionMonth>
                <Circle />
            </div>
            <div>
                <CompetiotionDate>14</CompetiotionDate>
                <CompetiotionMonth>декабря</CompetiotionMonth>
                <Circle />
            </div>
            <div>
                <CompetiotionDate>22</CompetiotionDate>
                <CompetiotionMonth>декабря</CompetiotionMonth>
                <Circle />
            </div>
            <div>
                <CompetiotionDate>11</CompetiotionDate>
                <CompetiotionMonth>января</CompetiotionMonth>
                <Circle />
            </div>
        </CompetiotionBlock>
        <CompetiotionBlock>
            <CompetiotionName href="#">Кубок Ренессанса 2018</CompetiotionName>
            <CompetiotionName href="#">Муниципальное соревнование</CompetiotionName>
            <CompetiotionName href="#">Кубок «Звездного вальса»</CompetiotionName>
            <CompetiotionName href="#">Танцевальный марафон8</CompetiotionName>
        </CompetiotionBlock>
    </Wrapper>
);
