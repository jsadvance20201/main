import React, { useState } from 'react';
import axios from 'axios';

import { PlayBtn } from '@main/assets';

import { TextInput } from '@main/components/textinput';
import { authHeader } from '@main/__data__/helpers/auth-header';
import { Wrapper, StyledText1, StyledText2, TextBlock, VideoBlock, Edit } from './styled';

// export class VideoSection2 extends React.PureComponent<any, any> {
//   state = {
//     isEdit: false,
//     titleText: this.props.data.title || 'Что такое портал учреждений досуга и спорта uds-moscow?'
//   };

//   toogleEdit= () => {
//     this.setState({isEdit: !this.state.isEdit});
//   }

//   setTitleText = (val) => {
//     this.setState({titleText: val});
//   }

//   saveTitle = (val) => {
//     let newData = this.props.data;

//     newData.title = val;

//     axios('/main/saveVideoSection', {
//       method: 'POST',
//       data: newData,
//       headers: {
//         'Content-Type': 'application/json',
//         ...authHeader()
//       }
//     }).then((res) => {

//       console.log('res', res);
//       this.forceUpdate();

//     })
//   }

//   render() {
//     const { isEdit, titleText }  = this.state;

//     return (
//       <Wrapper>
//         <VideoBlock >
//           <PlayBtn />
//         </VideoBlock>
//         <TextBlock>
//          {!isEdit && <StyledText1>
//             {titleText}
//         </StyledText1>}
//         {isEdit && <TextInput onBlur={this.saveTitle} value={titleText} onChange={this.setTitleText}/>}
//           <Edit onClick={this.toogleEdit} />
//           <StyledText2>
//             Давайте знакомиться! Посмотрите видео и, если
//             останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7
//             и ответить на все вопросы. Мы предложим спортивные, творческие и
//             развивающие кружки и секции для детей и взрослых любого возраста.
//         </StyledText2>
//         </TextBlock>
//       </Wrapper>
//     );
//   }
// }

export const VideoSection = ({data}) => {
  const [title, setTitle] = useState(data.title);
  const [descriptions, setDescriptions] = useState(data.descriptions);
  const [isEdit, setIsEdit] = useState(false);

  const toogleEdit = () => setIsEdit(!isEdit);

  return (
    <Wrapper>
      <VideoBlock >
        <PlayBtn />
      </VideoBlock>
      <TextBlock>
       {!isEdit && <StyledText1>
          {title}
      </StyledText1>}
      {isEdit && <TextInput onBlur={setTitle} value={title} onChange={setTitle}/>}
        <Edit onClick={toogleEdit} />
        <StyledText2>
          Давайте знакомиться! Посмотрите видео и, если
          останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7
          и ответить на все вопросы. Мы предложим спортивные, творческие и
          развивающие кружки и секции для детей и взрослых любого возраста.
      </StyledText2>
      </TextBlock>
    </Wrapper>
  );
};