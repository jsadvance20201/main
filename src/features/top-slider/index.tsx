import * as React from 'react';
import { connect } from 'react-redux';

import SliderCounter from './components/sliderCounter';
import { StyledImgSlider, Wrapper, StyledTextSlider, StyledText1, StyledText2, StyledButton } from './styled';
import { Slide, TransitionState } from './model';
import { throttle } from 'lodash';

/**
 * @prop {Array<Slide>} slides Слайды.
 * @prop {Function} onAction Обработчик главного действия.
 */
interface TopSliderProps {
    slides: Array<Slide>;
    onAction: (signData: any) => void;
}

/**
 * @prop {number} currentSlideIndex Текущий слайд.
 * @prop {number} prevSlideIndex Предыдущий слайд.
 * @prop {number} autoSwitchTimerId Таймер автоматического переключения слайдов.
 * @prop {number} endTransitionTimerId Таймер завершения анимации перехода.
 * @prop {TransitionState} transitionState Состояние перехода.
 */
interface TopSliderState {
    currentSlideIndex: number;
    nextSliderIndex: number;
    autoSwitchTimerId: number;
    endTransitionTimerId: number;
    transitionState: TransitionState;
}

const TRANSITION_DISTANCE = 70;

class TopSliderComponent extends React.PureComponent<TopSliderProps, TopSliderState> {
    state: TopSliderState = {
        currentSlideIndex: 0,
        nextSliderIndex: 0,
        autoSwitchTimerId: null,
        endTransitionTimerId: null,
        transitionState: TransitionState.IDLE,
    };

    componentDidMount() {
        const autoSwitchTimerId = window.setTimeout(this.handleStartAutoTransitrion, 3000);
        this.setState({ autoSwitchTimerId });
    }

    /**
     * Очистка всех таймеров.
     */
    clearTimeouts = () => {
        const { autoSwitchTimerId, endTransitionTimerId } = this.state;

        window.clearTimeout(autoSwitchTimerId);
        window.clearTimeout(endTransitionTimerId);
        this.setState({ autoSwitchTimerId: null, endTransitionTimerId: null });
    }

    /**
     * Обработчик начала автоматической смены слайдов.
     */
    handleStartAutoTransitrion = () => {
        this.handleNextSlideClick();
    }

    /**
     * Обработчик начала перехода.
     */
    handleStartTransition = () => {
        this.clearTimeouts();
        this.setState({ transitionState: TransitionState.DESOLVE });

        const endTransitionTimerId = window.setTimeout(this.handleEndTransition, 1500);
        this.setState({ endTransitionTimerId });
    }

    /**
     *  Обработчик завершения перехода.
     */
    handleEndTransition = () => {
        this.setState(prevState => ({ currentSlideIndex: prevState.nextSliderIndex, transitionState: TransitionState.IDLE }));
        this.clearTimeouts();

        const nextAutoSwitchTimerId = window.setTimeout(this.handleStartAutoTransitrion, 3000);
        this.setState({ autoSwitchTimerId: nextAutoSwitchTimerId });
    }

    /**
     * Устанавливает выбранный слайд по индексу.
     *
     * @param {number} index Индекс выбранного слайда.
     */
    setSlideByIndex = (index: number) => {
        this.clearTimeouts();
        this.setState({ nextSliderIndex: index }, this.handleStartTransition);
    }

    /**
     * Устанавливает слайд на следующий.
     */
    handleNextSlideClick = () => {
        const { slides } = this.props;
        const { nextSliderIndex } = this.state;
        let nextIndex = 0;
        this.clearTimeouts();

        if (nextSliderIndex + 1 !== slides.length) {
            nextIndex = nextSliderIndex + 1;
        }

        this.setState({ nextSliderIndex: nextIndex }, this.handleStartTransition);
    }

    /**
     * Устанавливает слайд на предыдущий.
     */
    handlePrevSlideClick = () => {
        const { slides } = this.props;
        const { nextSliderIndex } = this.state;

        this.clearTimeouts();
        let nextIndex = nextSliderIndex - 1;

        if (nextSliderIndex === 0) {
            nextIndex = slides.length - 1;
        }

        this.setState({ nextSliderIndex: nextIndex }, this.handleStartTransition);
    }

    componentWillUnmount() {
        this.clearTimeouts();
    }

    handleSignClick = () => {
        const { onAction } = this.props;
    }

    /**
     * Обработчик свайпа.
     *
     * @param {boolean} [isNext] Указывает следующее(true) или предыдущее(false) направление перелистывания.
     */
    handleSwipe = throttle(
        (isNext: boolean = false) => {
            if (isNext === true) {
                this.handleNextSlideClick();
            } else {
                this.handlePrevSlideClick();
            }
        },
        500,
        { trailing: false }
    );

    /**
     * Обработчик свайпа в левом направлении.
     */
    handleLeftSwipe = () => this.handleSwipe(true);

    render() {
        const { slides } = this.props;
        const { currentSlideIndex, transitionState, nextSliderIndex } = this.state;

        return (
            <Wrapper>
                <StyledTextSlider>
                    <StyledText1 transitionState={transitionState} transitionDist={TRANSITION_DISTANCE}>
                        {slides[currentSlideIndex].title}
                    </StyledText1>
                    <StyledText2 transitionState={transitionState} transitionDist={TRANSITION_DISTANCE}>
                        {slides[currentSlideIndex].message}
                    </StyledText2>
                    <div>
                        <StyledButton
                            transitionState={transitionState}
                            transitionDist={TRANSITION_DISTANCE}
                            onClick={this.handleSignClick}
                        >
                            Записаться
                        </StyledButton>
                    </div>
                </StyledTextSlider>
                <StyledImgSlider
                    transitionState={transitionState}
                    transitionDist={TRANSITION_DISTANCE}
                    onSwipedLeft={this.handleLeftSwipe}
                    onSwipedRight={this.handleSwipe}
                >
                    <img src={`${__webpack_public_path__}${slides[currentSlideIndex].picture}`} alt={slides[currentSlideIndex].alt} />
                </StyledImgSlider>
                <SliderCounter
                    onPrevSlide={this.handlePrevSlideClick}
                    onNextSlide={this.handleNextSlideClick}
                    goTo={this.setSlideByIndex}
                    currentSlide={nextSliderIndex}
                    slides={slides}
                />
            </Wrapper>
        );
    }
}

export const TopSlider = connect(null)(
    TopSliderComponent
);