import { createSelector } from 'reselect';

const getStore = state => state.dashboard;

const getmainData = createSelector(getStore, (state) => state.data);

export const getVideoSection = createSelector(getmainData, (state) => state?.videoSection);
export const getSlides = createSelector(getmainData, (state) => state?.slides);
export const getImgArr = createSelector(getmainData, (state) => state?.imgArr);

export const getMainDataLoading = createSelector(getStore, (state) => state?.loading);
export const getError = createSelector(getStore, (state) => state.error);
