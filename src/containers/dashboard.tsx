import * as React from 'react';
import MetaTags from 'react-meta-tags';
import { PageLoader } from '@ijl/uds-ui';
import { getConfig, getNavigationsValue } from '@ijl/cli';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Typography, Error } from '@ijl/uds-ui';

import getMainDataAction from '@main/__data__/actions/main';
import { getImgArr, getSlides, getVideoSection, getMainDataLoading, getError } from '@main/__data__/selectors/mainData';
import { socialLinks } from '@main/__data__/constants';
import {
  AreaIcons,
  Competitions,
  GallarySlider,
  SocialSection,
  TopSlider,
  VideoSection
} from '@main/features/exports';
import { Wrapper } from './styles/dashboard';

const App = ({ getMainData, imgArr, slides, videoSection, isLoading, error }) => {
  React.useEffect(() => {
    getMainData();
  },              []);

  const handleSignClick = (data) => {
    console.info('handleSignClick');
  };

  if (error) {
    return <Error link={getNavigationsValue('main')} />;
  }

  if (!imgArr || !slides || !videoSection || isLoading) {
    return <PageLoader />;
  }

  return (
    <Wrapper>
      <MetaTags>
        <title>Секции для детей</title>
        <meta name="description" content="Запишитесь на различные секции и занятия на нашем портале. Полный список спортивных городских учреждений и досуговых центров для детей и взрослых. Найдите свой центр в шаговой доступности." />
        <meta property="og:title" content="Главная" />
      </MetaTags>
      <React.Suspense fallback={<PageLoader />}>
        <TopSlider slides={slides} onAction={handleSignClick} />
        <AreaIcons />
        <VideoSection data={videoSection} />
        <GallarySlider array={imgArr} />
        <SocialSection links={socialLinks} />
        <Competitions />
      </React.Suspense>
    </Wrapper>
  );
};

const mapStateToProps = () => createStructuredSelector({
  imgArr: getImgArr,
  slides: getSlides,
  videoSection: getVideoSection,
  isLoading: getMainDataLoading,
  error: getError,
});

export default connect(mapStateToProps, { getMainData: getMainDataAction })(App);
