import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as Thunk from 'redux-thunk';

import reducer from '@main/__data__/reducers';
import Container from '@main/containers/dashboard';

export default () => (
  <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
    <Container />
  </Provider>
);
