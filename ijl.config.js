const pkg = require("./package.json");

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name.replace("uds-", "")}/${process.env.VERSION || pkg.version}/`
    }
  },
  config: {
    'main.api.base.url': '/api',
  },
  apps: {
    main: { name: 'main', version: process.env.VERSION || pkg.version }
  },
  navigations: {
    news: "/news",
    orgs: "/orgs",
    ya: "https://yandex.ru"
  }
};
